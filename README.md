# tp8 云存储
## 七牛云、腾讯云、阿里云
~~~
$file = request()->file('file'); //文件|字符串|文件流
//七牛上传
Filesystem::disk('qiniu')->putFile('upload', $file);
//腾讯上传
Filesystem::disk('cos')->putFile('upload', $file);
//阿里上传
Filesystem::disk('oss')->putFile('upload', $file);
~~~